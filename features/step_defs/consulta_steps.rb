Dado('que existe el equipo {string}') do |nombre|
  body = {
    'nombre_equipo': nombre,
    'presupuesto': 300
  }
  post '/equipos', body.to_json, 'CONTENT_TYPE' => 'application/json'
  @id_equipo = JSON.parse(last_response.body)['id']
end

Dado('que tiene {int} arqueros con potencial {int}') do |cantidad_arqueros, potencialidad|
  (0..cantidad_arqueros - 1).each do |_x|
    fichar_jugador('arquero', potencialidad)
  end
end

def fichar_jugador(posicion, potencialidad)
  body = {
    'nombre': "juan #{posicion}",
    'nacionalidad': 'argentino',
    'posicion': posicion,
    'valor_de_mercado': 10,
    'potencialidad': potencialidad
  }
  post "/equipos/#{@id_equipo}/jugadores", body.to_json, 'CONTENT_TYPE' => 'application/json'
end

Dado('que tiene {int} delanteros con potencial {int}') do |cantidad_delanteros, potencialidad|
  (0..cantidad_delanteros - 1).each do |_x|
    fichar_jugador('delantero', potencialidad)
  end
end

Cuando('consulto sus detalles') do
  get "/equipos/#{@id_equipo}"
end

Entonces('poder ofensivo es {int}') do |poder_ofensivo_esperado|
  expect(last_response.status).to be == 200
  poder_ofensivo = JSON.parse(last_response.body)['poder_ofensivo'].to_i
  expect(poder_ofensivo).to be == poder_ofensivo_esperado
end

Entonces('poder defensivo es {int}') do |poder_defensivo_esperado|
  expect(last_response.status).to be == 200
  poder_defensivo = JSON.parse(last_response.body)['poder_defensivo'].to_i
  expect(poder_defensivo).to be == poder_defensivo_esperado
end
